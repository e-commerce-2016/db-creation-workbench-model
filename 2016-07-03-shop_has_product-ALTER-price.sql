ALTER TABLE mobilizr.shop_has_product MODIFY price DECIMAL(15,2) NOT NULL;
